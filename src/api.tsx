import axios from "axios";

export interface IUser {
  FirstName: string;
  LastName: string;
  Email: string;
}

export const axiosInstance = axios.create({
  baseURL: "/api",
});

export const fetchUser = async (): Promise<IUser> => {
  const response = await axiosInstance.get(`/user`);

  return response.data;
};
