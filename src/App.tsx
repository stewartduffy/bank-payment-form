import React from "react";
import styled from "styled-components";
import { CardHeader, CardBody } from "./components/";

import { ThemeProvider } from "styled-components";
import { AppProvider } from "./AppContext";
import theme from "./theme";

const App = () => {
  return (
    <AppProvider>
      <ThemeProvider theme={theme}>
        <CardWrapper>
          <CardHeader />
          <CardBody />
        </CardWrapper>
      </ThemeProvider>
    </AppProvider>
  );
};

export const CardWrapper = styled.div`
  border: ${(props) => props.theme.lofiBorder};
  height: 390px;
  width: 278px;
`;

export default App;
