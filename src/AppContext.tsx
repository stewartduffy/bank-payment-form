import React, { createContext, useContext, ReactNode, useState } from "react";
import { fetchUser, IUser } from "./api";

interface AppContextInterface {
  isMenuOpen: boolean;
  user: IUser | null;
  isLoading: boolean;
  openMenu: () => void;
  closeMenu: () => void;
  fetchData: () => void;
}

const AppContext = createContext<AppContextInterface | null>(null);

function useAppContext() {
  const context = useContext(AppContext);

  if (!context) {
    throw new Error(`useAppContext must be used within a AppProvider`);
  }
  return context;
}

type AppProviderProps = {
  children: ReactNode;
};

function AppProvider(props: AppProviderProps) {
  const [isMenuOpen, setIsOpen] = useState(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [user, setUser] = useState<IUser | null>(null);

  const openMenu = () => {
    setIsOpen(true);
  };

  const closeMenu = () => {
    setIsOpen(false);
  };

  const fetchData = () => {
    setIsLoading(true);
    (async function () {
      const user = await fetchUser();
      setIsLoading(false);
      setUser(user);
    })();
  };

  return (
    <AppContext.Provider
      value={{ isMenuOpen, user, isLoading, fetchData, openMenu, closeMenu }}
      {...props}
    />
  );
}

export { AppProvider, useAppContext };
