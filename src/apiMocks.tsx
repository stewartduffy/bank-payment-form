import MockAdapter from "axios-mock-adapter";
import { axiosInstance } from "./api";

const mock = new MockAdapter(axiosInstance, { delayResponse: 500 });

mock.onGet("/user").reply(200, {
  FirstName: "Stewart",
  LastName: "Duffy",
  Email: "duffy.stewart@gmail.com",
});
