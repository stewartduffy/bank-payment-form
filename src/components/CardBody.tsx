import React, { Fragment } from "react";
import styled from "styled-components";
import PaymentForm from "./PaymentForm";
import { useMount } from "../hooks";
import { useAppContext } from "../AppContext";

/**
 * We include api so we can build against an API without it being built yet.
 */
import "../apiMocks";

const CardBody = () => {
  const { isMenuOpen, fetchData, isLoading, user } = useAppContext();

  useMount(() => {
    fetchData();
  });

  if (isLoading) {
    return (
      <Fragment>
        <StyledCardBody>
          <p>Loading...</p>
        </StyledCardBody>
      </Fragment>
    );
  }

  if (isMenuOpen) {
    return (
      <Fragment>
        <StyledCardBody>
          <p>This is menu content</p>
        </StyledCardBody>
      </Fragment>
    );
  }

  return (
    <Fragment>
      <StyledCardBody>
        {user != null && <p>Welcome {user?.FirstName}</p>}
        <PaymentForm />
      </StyledCardBody>
    </Fragment>
  );
};

export const StyledCardBody = styled.div`
  padding: 22px;
`;

export default CardBody;
