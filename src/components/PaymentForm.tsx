import React from "react";
import { Formik, Form } from "formik";
import { parse, isDate } from "date-fns";
import * as Yup from "yup";
import styled from "styled-components";
import FormInput from "./FormInput";
import ValidationMessage from "./ValidationMessage";

function parseDateString(value: string, originalValue: string) {
  const parsedDate = isDate(originalValue)
    ? originalValue
    : parse(originalValue, "MM-yyyy", new Date());

  return parsedDate;
}

const PaymentSchema = Yup.object().shape({
  cardNumber: Yup.number()
    .typeError("Card number must be a number")
    .required("Card number is incomplete"),
  cvc: Yup.number()
    .typeError("CVC must be a number")
    .required("CVC is incomplete"),
  expiry: Yup.date()
    .typeError("Expiry must be a date (mm-yy)")
    .required("Expiry is incomplete")
    .transform(parseDateString),
});

const PaymentForm = () => {
  return (
    <Formik
      initialValues={{
        cardNumber: "",
        cvc: "",
        expiry: "",
      }}
      validationSchema={PaymentSchema}
      onSubmit={async (values) => {
        await new Promise((r) => setTimeout(r, 500));
        console.log(JSON.stringify(values, null, 2));
      }}
    >
      <Form>
        <FormGrid>
          <FormInput
            name="cardNumber"
            className="cardNumber"
            placeholder="Credit Card Number"
          />
          <FormInput name="cvc" className="cvc" placeholder="CVC" />
          <FormInput name="expiry" className="expiry" placeholder="expiry" />
          <FormButton type="submit" className="submit">
            Submit
          </FormButton>
        </FormGrid>
        <ValidationMessage name="cardNumber" />
        <ValidationMessage name="cvc" />
        <ValidationMessage name="expiry" />
      </Form>
    </Formik>
  );
};

export const FormButton = styled.button`
  display: block;
  width: 100%;
  height: 27px;
  background: none;
  border: ${(props) => props.theme.lofiBorder};
`;

export const FormGrid = styled.div`
  width: 100%;
  display: grid;
  grid-gap: 15px;
  grid-template-columns: repeat(3, minmax(0, 1fr));
  grid-template-areas:
    "cardNumber cardNumber cardNumber"
    ". cvc expiry"
    "submit submit submit";

  .cardNumber {
    grid-area: cardNumber;
  }
  .cvc {
    grid-area: cvc;
  }
  .expiry {
    grid-area: expiry;
  }
  .submit {
    grid-area: submit;
  }
`;

export default PaymentForm;
