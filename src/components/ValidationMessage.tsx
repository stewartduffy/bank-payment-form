import React from "react";
import { ErrorMessage } from "formik";
import styled from "styled-components";

type ValidationMessageProps = {
  name: string;
};

const ValidationMessage = ({ name }: ValidationMessageProps) => {
  return <ErrorMessage name={name} component={StyledValidationMessage} />;
};

export const StyledValidationMessage = styled.p`
  color: red;
`;

export default ValidationMessage;
