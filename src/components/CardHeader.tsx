import React, { Fragment } from "react";
import styled from "styled-components";

import { FaBars } from "react-icons/fa";
import { FaArrowLeft } from "react-icons/fa";
import { useAppContext } from "../AppContext";

const CardHeader = () => {
  const { isMenuOpen, openMenu, closeMenu } = useAppContext();
  const handleClick = isMenuOpen ? closeMenu : openMenu;
  const Icon = isMenuOpen ? FaArrowLeft : FaBars;
  const text = isMenuOpen ? "Menu" : "Register card form";

  return (
    <StyledFormHeader>
      <Fragment>
        <FormHeaderButton type="button" onClick={handleClick}>
          <Icon />
        </FormHeaderButton>
        <p>{text}</p>
      </Fragment>
    </StyledFormHeader>
  );
};

export default CardHeader;

export const StyledFormHeader = styled.div`
  border-bottom: ${(props) => props.theme.lofiBorder};
  height: 48px;
  display: flex;
  align-items: center;
`;

export const FormHeaderButton = styled.button`
  background: none;
  border: none;
  cursor: pointer;
  padding: 20px;
`;
