import React from "react";
import { Field } from "formik";
import styled from "styled-components";

type FormInputProps = {
  name: string;
  placeholder?: string;
  className?: string;
};

const FormInput = ({ name, placeholder, className }: FormInputProps) => {
  return (
    <Field
      type="text"
      name={name}
      className={className}
      placeholder={placeholder}
      as={StyledFormInput}
    />
  );
};

export const StyledFormInput = styled.input`
  display: block;
  height: 27px;
  border: ${(props) => props.theme.lofiBorder};

  &::placeholder {
    color: #000;
  }
`;

export default FormInput;
